package treeout;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tml.output.TMLOutput;
import tml.parser.Parser;
import tml.tree.GenericNode;

public class TreeOutSimpleTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1STR = "<1>";
    private static final GenericNode TREE1 = Parser.makeTree(TREE1STR, 2);

    @Test(timeout = TIMEOUT)
    public void testTreeOut1() {
        assertEquals(TREE1, Parser.makeTree(
                TMLOutput.stringMarkUpTree(TREE1, 2), 2));
    }

    private static final String TREE2STR
        = "<1>\n"
        + "  <2>";
    private static final GenericNode TREE2 = Parser.makeTree(TREE2STR, 2);

    @Test(timeout = TIMEOUT)
    public void testTreeOut2() {
        assertEquals(TREE2, Parser.makeTree(
                TMLOutput.stringMarkUpTree(TREE2, 2), 2));
    }

    private static final String TREE3STR
        = "<null>\n"
        + "  <\"null\">";
    private static final GenericNode TREE3 = Parser.makeTree(TREE3STR, 2);

    @Test(timeout = TIMEOUT)
    public void testTreeOut3() {
        assertEquals(TREE3, Parser.makeTree(
                TMLOutput.stringMarkUpTree(TREE3, 2), 2));
    }

    private static final String TREE4STR
        = "<#, 2>\n"
        + "  <\\\"Hi\\\", \"Hello, world!\", How are you?>\n"
        + "    <>";
    private static final GenericNode TREE4 = Parser.makeTree(TREE4STR, 2);

    @Test(timeout = TIMEOUT)
    public void testTreeOut4() {
        assertEquals(TREE4, Parser.makeTree(
                TMLOutput.stringMarkUpTree(TREE4, 2), 2));
    }

}
