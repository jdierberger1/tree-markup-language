# Tree Markup Language (TML)
### https://gitlab.com/jdierberger1/tree-markup-language
### Version 1.0.2

**A simple markup language oriented around the tree data structure.**

## Rules of the Language
### File extensions
TML recognizes the .tml format by default. Files which conform to the TML
language specification can be read by TML, but are not officially supported.
The TML file format specification is as follows:
- .tml files may not contain any null characters (\u0000) which can be read by
the parser (operating system null terminators are allowed for obvious reasons)
- The usage of tabs, while currently recognized by the parser, is not
officially supported by TML and may be deprecated for deletion in the future.
- All configuration options must be present before any trees.
A .tml file contains two parts:
- The **Configuration options**, which specify how to parse a file, and
- the **Forest**, which contains the tree(s) to parse.

### Configuration options
Configuration options are given at the top of a .tml file. Configuration
options tell the TML file parser how it should parse the forest contained in a
given .tml file. Configuration options must be delimited from the forest by one
or more empty lines.
- **indentation_per_level**: See **ipl**.
- **ipl**: Indentation per level. Specifies how much indentation (in spaces) is
required to change the level of a node in a tree. **ipl** has a default value
of 2.

### Comments
A comment is any line starting with a "#" character, preceded by any
whitespace. A comment **must** be on its own line and cannot exist on a line
with a node; while the parser may not inherently error if such comments exist
this behavior is undefined and not supported and may be explicitly forbidden
in the future.

### Format of a node
A node stores data and has 'children' nodes. In TML, nodes can store a single
datum alongside multiple attributes. A simple node with no attributes is
formatted as follows:
`<data>`
where data is the data to be stored. Datatypes are inferred in TML, or stored
as a String if the datatype cannot be inferred. For more information, see the
Data types section of Functionality of TML.

A node which contains attributes in addition to data is formatted as follows:
`<data, attribute0, attribte1, ...>`
where a the comma separated list of attributes can contain the same types
as `data`. A node can have as many attributes as desired. Furthermore, while
all nodes have data (or null), nodes in a tree are not all required to have
the same attributes or number of attributes.

### Format of a tree
A tree is made up of nodes. These trees are structured exactly like trees in
any other mark-up language, namely that:
- nodes are delimited by line terminators;
- nodes are translated from top-down to left-right in an actual tree
data-structure;
- a node is a child of the nearest node in the file for which it is indented
by exactly one indentation level, as prescribed in **ipl**.
For example, with **ipl** = 2, the following tree (in vertical view)

```
         5
    |----|----|
    2    4    8
    | |-----|  
    1 3     6
            |
            7
```

would become:

```
<5>
  <2>
    <1>
  <4>
    <3>
    <6>
      <7>
  <8>
```

in a .tml file. Note how left-right nodes in the tree are arranged vertically,
and how children are indented from their parents. All indentation must increase
by one level per line. While the indentation can decrease by any amount it can
only increase by one indentation level per line.

### Forests
A forest in a .tml file is simply a collection of trees. A forest consists of
zero or more trees. All trees in a forest must be delimited by one or more
empty lines. A comment does not qualify as an empty line.

## Functionality of TML
### Data types
TML is capable of recognizing and parsing a variety of data-types. The types
TML supports inherently is:
- `null`
- Integer (whole number) types (including 32-bit, 64-bit, and unlimited
precision)
- Floating point (real number) types (currently 64-bit only)
- Strings

These data-types are recognized in the following formats:

**Integer:**
- Binary (base 2), recognized by the prefix `0b` before the number.
- Decimal (base 10), the default numeric type.
- Hexadecimal (base 16), recognized by the prefix `0x` before the number.

**Floating point:**
- Floating point numbers are recognized by the presence of a radix-point (`.`).

**null:**
- A node formatted as `<null>` is regarded to have null data.
- A node formatted as `<>` is regarded to have null data.

**String:**
- All other data not satisfying the above constraints is stored as a String.
- Strings can also be forced by adding quotations ("s) around the data (e.g
`<"null">`)

### Generic trees
TML stores trees in memory as a **generic tree**. Generic trees are capable of
supporting all data-types listed above, holding a singular datum, unlimited
attributes, and unlimited children. These trees are generated from the TML
parser to store a tree in memory, and used by the TMLOuput class to output
trees.