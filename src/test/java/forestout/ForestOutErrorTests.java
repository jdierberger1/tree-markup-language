package forestout;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
//import org.junit.Test;
import org.junit.rules.ExpectedException;

import tml.output.TMLOutput;
import tml.tree.GenericNode;

public class ForestOutErrorTests {

    @SuppressWarnings("unused")
    private static final int TIMEOUT = 200;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

//    @Test(timeout = TIMEOUT)
    public void testForestOut1() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.stringMarkUpForest(2, (GenericNode[]) null);
    }

//    @Test(timeout = TIMEOUT)
    public void testForestOut2() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.printMarkUpForest(2, null, new GenericNode());
    }

//    @Test(timeout = TIMEOUT)
    public void testForestOut3() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.printMarkUpForest(2, System.out, (GenericNode[]) null);
    }

//    @Test(timeout = TIMEOUT)
    public void testForestOut4() throws IOException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.writeMarkUpForest(2, null, new GenericNode());
    }

//    @Test(timeout = TIMEOUT)
    public void testForestOut5() throws IOException {
        File file = new File("./src/test/out/error_should_be_empty.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.containsString("null"));
        TMLOutput.writeMarkUpForest(2, new FileWriter(file),
                (GenericNode[]) null);
    }

}
