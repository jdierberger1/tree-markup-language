package maketree;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class MakeTreeWithAttributesTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1
            = "<0, 0>\n"
            + "  <1, 1>\n"
            + "    <2, 2>\n"
            + "  <3, 3>\n";

    @Test(timeout = TIMEOUT)
    public void testMakeTree1() {
        GenericNode root = Parser.makeTree(TREE1, 2);
        assertEquals(Integer.valueOf(0), root.getData());
        assertEquals(Integer.valueOf(1), root
                .getChildren()[0]
                .getData());
        assertEquals(Integer.valueOf(3), root
                .getChildren()[1]
                .getData());
        assertEquals(Integer.valueOf(2), root
                .getChildren()[0]
                .getChildren()[0]
                .getData());

        assertEquals(Integer.valueOf(1),
                Integer.valueOf(root.getAttributes().length));
        assertEquals(Integer.valueOf(1), Integer.valueOf(root
                .getChildren()[0]
                .getAttributes().length));
        assertEquals(Integer.valueOf(1), Integer.valueOf(root
                .getChildren()[1]
                .getAttributes().length));
        assertEquals(Integer.valueOf(1), Integer.valueOf(root
                .getChildren()[0]
                .getChildren()[0]
                .getAttributes().length));

        assertEquals(Integer.valueOf(0), root.getAttribute(0));
        assertEquals(Integer.valueOf(1), root
                .getChildren()[0]
                .getAttribute(0));
        assertEquals(Integer.valueOf(3), root
                .getChildren()[1]
                .getAttribute(0));
        assertEquals(Integer.valueOf(2), root
                .getChildren()[0]
                .getChildren()[0]
                .getAttribute(0));
    }

    private static final String TREE2
            = "<null, null>\n"
            + "  <null, Hello!>\n"
            + "  <null, \"null\">\n"
            + "  <null, \\\"Ex\\\">\n"
            + "  <null, 1.0>\n"
            + "  <null, 1, 2>\n"
            + "  <null, \"1\">\n"
            + "  <null, \"<>\">\n"
            + "  # comment # \"\n"
            + "  <null, \"#\">\n"
            + "  <\"Hello, world!\">\n"
            + "  <Hello, world!>\n"
            + "  <Hello, \"world, how are you?\">\n";

    @Test(timeout = TIMEOUT)
    public void testMakeTree2() {
        GenericNode root = Parser.makeTree(TREE2, 2);
        assertEquals(null, root.getAttribute(0));
        assertEquals("Hello!", root.getChild(0).getAttribute(0));
        assertEquals("null", root.getChild(1).getAttribute(0));
        assertEquals("\"Ex\"", root.getChild(2).getAttribute(0));
        assertEquals(Double.valueOf(1.0), root.getChild(3).getAttribute(0));
        assertEquals(Integer.valueOf(1), root.getChild(4).getAttribute(0));
        assertEquals(Integer.valueOf(2), root.getChild(4).getAttribute(1));
        assertEquals("1", root.getChild(5).getAttribute(0));
        assertEquals("<>", root.getChild(6).getAttribute(0));
        assertEquals("#", root.getChild(7).getAttribute(0));
        assertEquals("Hello, world!", root.getChild(8).getData());
        assertEquals("Hello", root.getChild(9).getData());
        assertEquals("world!", root.getChild(9).getAttribute(0));
        assertEquals("Hello", root.getChild(10).getData());
        assertEquals("world, how are you?", root.getChild(10).getAttribute(0));
    }

}
