package makeforest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class MakeForestSimpleTests {

    private static final int TIMEOUT = 200;

    private static final String TREESTR1 = "<2>\n";

    private static final GenericNode TREE1 = Parser.makeTree(TREESTR1, 2);

    private static final String TREESTR2
            = "<2>\n"
            + "  <2>\n";

    private static final GenericNode TREE2 = Parser.makeTree(TREESTR2, 2);

    private static final String TREESTR3
        = "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "  <2>\n";

    private static final GenericNode TREE3 = Parser.makeTree(TREESTR3, 2);

    @Test(timeout = TIMEOUT)
    public void testMakeTree1() {
        List<GenericNode> list = Parser.makeForest(TREESTR1, 2);
        assertEquals(Arrays.asList(TREE1), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree2() {
        List<GenericNode> list = Parser.makeForest(TREESTR2, 2);
        assertEquals(Arrays.asList(TREE2), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree3() {
        List<GenericNode> list = Parser.makeForest(TREESTR3, 2);
        assertEquals(Arrays.asList(TREE3), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeForest1() {
        List<GenericNode> list = Parser.makeForest(TREESTR1
                + "\n" + TREESTR2, 2);
        assertEquals(Arrays.asList(TREE1, TREE2), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeForest2() {
        List<GenericNode> list = Parser.makeForest(TREESTR1
                + "\n" + TREESTR3, 2);
        assertEquals(Arrays.asList(TREE1, TREE3), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeForest3() {
        List<GenericNode> list = Parser.makeForest(TREESTR2
                + "\n" + TREESTR3, 2);
        assertEquals(Arrays.asList(TREE2, TREE3), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeForest4() {
        List<GenericNode> list = Parser.makeForest(TREESTR1
                + "\n" + TREESTR2 + "\n" + TREESTR3, 2);
        assertEquals(Arrays.asList(TREE1, TREE2, TREE3), list);
    }

    @Test(timeout = TIMEOUT)
    public void testMakeForestEmpty() {
        List<GenericNode> list = Parser.makeForest("", 2);
        assertEquals(Arrays.asList(), list);
    }

}
