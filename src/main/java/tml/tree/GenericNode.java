package tml.tree;

/**
 * Generic Tree Node. These nodes support a simple tree structure, where each
 * node can hold an arbitrary amount of data and have arbitrarily many
 * children.<br>
 * <br>
 * These nodes will initially maintain two children but can hold as many
 * children as can be allocated. Null children are supported, but will only
 * exist as a side-effect in the unused portion of the array that maintains
 * the children of this node and cannot be viewed or acquired by the end user
 * of this class. Two children are distinguished; the left child is
 * distinguished to be the child at index 0 in the array maintaining a node's
 * children (or null if no such children exist), and the right child is
 * distinguished to be the child at the index one less than the number of
 * children the node maintains.<br>
 * <br>
 * While the node is able to maintain as much data as can be allocated, the
 * data this node supports is recognized in two partitions. One datum is
 * recognized as being the primary data, or just the <i>data</i> of the node.
 * The remaining information a node holds is regarded as being secondary data,
 * or <i>attributes</i> of the node. These attributes can be considered
 * contents of the node, or as meta-data of the node (such as storing the
 * height of a node).
 * @author J-Dierberger
 * @version 1.0
 */
public class GenericNode {

    // Array maintaining this node's children.
    private GenericNode[] children;

    // How many children this node maintains.
    private int numChildren;

    // The primary datum (data) of this node.
    private final Object data;

    // The secondary data (attributes) of this node.
    private final Object[] attributes;

    /**
     * Arrays maintained by generic nodes resize with a ratio of
     * (1 + sqrt(5)) / 2 (i.e phi, the golden ratio).
     */
    private static final double PHI = 1.618033988749894848204586834;

    /**
     * Create a GenericNode with null data and no attributes or children.
     */
    public GenericNode() {
        this(null);
    }

    /**
     * Create a GenericNode with the given data and attributes and no children.
     * If an array is passed in for attributes, the array will be cloned to
     * maintain the referential integrity of the node and of the creator of
     * this node.
     * @param data The data this node maintains.
     * @param attributes The attributes this node maintains.
     */
    public GenericNode(Object data, Object...attributes) {
        this.data = data;
        children = new GenericNode[2];
        this.attributes = attributes.clone();
    }

    /**
     * Add the given GenericNodes as children of this node. These nodes are
     * added from left to right.
     * @param nodes The nodes to add as children.
     */
    public void addChildren(GenericNode...nodes) {
        for (GenericNode node : nodes) {
            addChild(node);
        }
    }

    /**
     * Add the given GenericNode as a child of this node. The node added will
     * become the rightmost child of the current node.
     * @param node The node to add as the rightmost child.
     */
    private void addChild(GenericNode node) {
        if (numChildren >= children.length) {
            GenericNode[] newArr = new GenericNode[
                (int) Math.round(children.length * PHI)
            ];
            for (int i = 0; i < children.length; i++) {
                newArr[i] = children[i];
            }
            children = newArr;
        }
        children[numChildren++] = node;
    }

    /**
     * Get the children of this node. The array returned will not contain any
     * extra null spaces that the array maintained by this node has. Like in
     * the array maintained by this node, the leftmost child will be at index 0
     * in the array, and the rightmost child will be at the last valid index in
     * the array.
     * @return A GenericNode array which contains children of this node.
     */
    public GenericNode[] getChildren() {
        GenericNode[] children = new GenericNode[numChildren];
        for (int i = 0; i < numChildren; i++) {
            children[i] = this.children[i];
        }
        return children;
    }

    /**
     * Get the nth child of this node.
     * @param n The number of the child to get.
     * @return The nth child of this node.
     * @throws IndexOutOfBoundsException If the desired index does not
     * correspond to a child of this node.
     */
    public GenericNode getChild(int n) {
        if (n < 0 || n >= numChildren) {
            throw new IndexOutOfBoundsException(
                String.format("Node with %d children doesn't have %dth child.",
                        numChildren, n)
            );
        }
        return children[n];
    }

    /**
     * Get the leftmost child of this node, or null if this node has no
     * children.
     * @return The leftmost child of this node.
     */
    public GenericNode getLeftChild() {
        return children[0];
    }

    /**
     * Get the rightmost child of this node, or null if this node has no
     * children.
     * @return The rightmost child of this node.
     */
    public GenericNode getRightChild() {
        return numChildren == 0 ? null : children[numChildren - 1];
    }

    /**
     * Get the data from this node.
     * @return The data from this node.
     */
    public Object getData() {
        return data;
    }

    /**
     * Get the attributes of this node. Returns a clone of the array maintained
     * by this node to maintain the referential integrity of the node.
     * @return The attributes of this node.
     */
    public Object[] getAttributes() {
        return attributes.clone();
    }

    /**
     * Get the nth attribute of this node.
     * @param n The number of the attribute to get.
     * @return The nth attribute of this node.
     * @throws IndexOutOfBoundsException If the desired index does not
     * correspond to an attribute of this node.
     */
    public Object getAttribute(int n) {
        if (n < 0 || n >= attributes.length) {
            throw new IndexOutOfBoundsException(
                String.format(
                    "Node with %d attributes doesn't have %dth attribute.",
                    numChildren, n)
            );
        }
        return attributes[n];
    }

    /**
     * Get the number of children this node maintains.
     * @return The number of children this node maintains.
     */
    public int getNumChildren() {
        return numChildren;
    }

    /**
     * Get the number of attributes this node maintains.
     * @return The number of attributes this node maintains.
     */
    public int getNumAttributes() {
        return attributes.length;
    }

    /**
     * {@inheritDoc}
     * <br>
     * Two nodes are considred to be equal if:
     * <ul>
     * <li> They have the same data
     * <li> They have the same attributes
     * <li> All their children are equal and appear in the same left to right
     * order.
     * </ul>
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof GenericNode)) {
            return false;
        }
        GenericNode g = (GenericNode) o;
        if ((g.data == null) != (data == null)) {
            return false;
        }
        if (g.attributes.length != attributes.length) {
            return false;
        }
        for (int i = 0; i < attributes.length; i++) {
            if (!attributes[i].equals(g.attributes[i])) {
                return false;
            }
        }
        if (g.numChildren != numChildren) {
            return false;
        }
        for (int i = 0; i < numChildren; i++) {
            if ((g.children[i] == null) != (children[i] == null)) {
                return false;
            }
            if (children[i] != null && !children[i].equals(g.children[i])) {
                return false;
            }
        }
        return g.data == null || g.data.equals(data);
    }

    /**
     * {@inheritDoc}
     * <br>
     * The hash code of a GenericNode is the hash code of its data, bitwise
     * XOR-ed with the constant 0x12349876
     */
    @Override
    public int hashCode() {
        return 0x12349876 ^ data.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder()
            .append("<")
            .append(format(data));
        for (int i = 0; i < attributes.length; i++) {
            s.append(", ")
                .append(format(attributes[i]));
        }
        s.append(">");
        return s.toString();
    }

    /**
     * Check if the given object is equal to the data maintained by this node.
     * @param g The object to check.
     * @return If this node's data is equal to the given object..
     */
    public boolean dataEquals(Object g) {
        return (data == null && g == null) || (g != null && g.equals(data));
    }

    /**
     * Format data for printing.
     * @param data The data to format.
     * @return The data, formatted for printing as TML data for parsing.
     */
    private static Object format(Object data) {
        if (data == null) {
            return "null";
        } else if (data.getClass() == String.class) {
            return "\""
                + data.toString()
                    .replace("\\", "\\\\")
                    .replace("\n", "\\n")
                    .replace("\t", "\\t")
                    .replace("\r", "\\r")
                    .replace("\"", "\\\"")
                + "\"";
        } else {
            return data;
        }
    }

}
