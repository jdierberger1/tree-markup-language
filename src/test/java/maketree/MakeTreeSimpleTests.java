package maketree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class MakeTreeSimpleTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1 =
            "<0>\n"
                    + "  <1>\n"
                    + "  <2>\n";

    private static final String TREE2 =
            "<0>\n"
                    + "   <1>\n"
                    + "   <2>\n";

    private static final String TREE3 =
            "<0>\n"
                    + "\t<1>\n"
                    + "\t<2>\n";

    private static final String TREE4 =
            "<0>\n"
                    + "  <1>\n"
                    + "    <2>\n"
                    + "      <3>\n";

    private static final String TREE5 =
            "<0>\n"
                    + "  <i>\n"
                    + "  <2>\n"
                    + "  <3>\n"
                    + "  <4>\n"
                    + "  <\\\"V\\\">\n"
                    + "  <6>\n"
                    + "  <\"7\">\n"
                    + "  <8>\n"
                    + "  <9>\n"
                    + "  <>\n";

    @Test(timeout = TIMEOUT)
    public void testMakeTree1() {
        GenericNode root = Parser.makeTree(TREE1, 2);
        assertEquals(Integer.valueOf(0), root.getData());
        assertEquals(Integer.valueOf(1), root.getChildren()[0].getData());
        assertEquals(Integer.valueOf(2), root.getChildren()[1].getData());
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree2() {
        GenericNode root = Parser.makeTree(TREE2, 3);
        assertEquals(Integer.valueOf(0), root.getData());
        assertEquals(Integer.valueOf(1), root.getChildren()[0].getData());
        assertEquals(Integer.valueOf(2), root.getChildren()[1].getData());
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree3() {
        GenericNode root = Parser.makeTree(TREE3, 4);
        assertEquals(Integer.valueOf(0), root.getData());
        assertEquals(Integer.valueOf(1), root.getChildren()[0].getData());
        assertEquals(Integer.valueOf(2), root.getChildren()[1].getData());
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree4() {
        GenericNode root = Parser.makeTree(TREE4, 2);
        assertEquals(Integer.valueOf(0), root.getData());
        assertEquals(Integer.valueOf(1), root.getChildren()[0].getData());
        assertEquals(Integer.valueOf(2), root.getChildren()[0]
                .getChildren()[0].getData());
        assertEquals(Integer.valueOf(3), root.getChildren()[0]
                .getChildren()[0]
                .getChildren()[0].getData());
    }

    @Test(timeout = TIMEOUT)
    public void testMakeTree5() {
        GenericNode root = Parser.makeTree(TREE5, 2);
        assertEquals(Integer.valueOf(0), root.getData());
        for (GenericNode node : root.getChildren()) {
            assertNotNull(node);
        }
        assertEquals("i", root.getChildren()[0].getData());
        assertEquals(Integer.valueOf(2), root.getChildren()[1].getData());
        assertEquals(Integer.valueOf(3), root.getChildren()[2].getData());
        assertEquals(Integer.valueOf(4), root.getChildren()[3].getData());
        assertEquals("\"V\"", root.getChildren()[4].getData());
        assertEquals(Integer.valueOf(6), root.getChildren()[5].getData());
        assertEquals("7", root.getChildren()[6].getData());
        assertEquals(Integer.valueOf(8), root.getChildren()[7].getData());
        assertEquals(Integer.valueOf(9), root.getChildren()[8].getData());
        assertEquals(null, root.getChildren()[9].getData());
    }

}
