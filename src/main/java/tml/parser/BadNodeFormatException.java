package tml.parser;

/**
 * Indicates a node formatting issue during parsing which causes the node to be
 * un-parseable
 * @author J-Dierberger
 * @version 1.0
 */
public class BadNodeFormatException extends RuntimeException {

    /**
     * UID.
     */
    private static final long serialVersionUID = 5248883674731546304L;

    /**
     * Create a BadNodeFormatException.
     * @param msg The exception message.
     */
    public BadNodeFormatException(String msg) {
        super(msg);
    }

}
