/**
 * This package contains components to create and store different types of
 * trees, as well as providing utilities for trees.
 * @author J-Dierberger
 */
package tml.tree;