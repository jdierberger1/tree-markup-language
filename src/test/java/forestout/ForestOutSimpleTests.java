package forestout;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import tml.output.TMLOutput;

//import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class ForestOutSimpleTests {

    @SuppressWarnings("unused")
    private static final int TIMEOUT = 200;

    private static final String TREE1STR = "<1>";
    private static final GenericNode TREE1 = Parser.makeTree(TREE1STR, 2);

    private static final String TREE2STR
        = "<1>\n"
        + "  <2>";
    private static final GenericNode TREE2 = Parser.makeTree(TREE2STR, 2);

//    @Test(timeout = TIMEOUT)
    public void testForestOut1() throws IOException {
        File file = new File("./src/test/out/simple_out_1.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(2, new FileWriter(file), TREE1);
        assertEquals(Arrays.asList(TREE1), Parser.parseFile(file));
    }

//    @Test(timeout = TIMEOUT)
    public void testForestOut2() throws IOException {
        File file = new File("./src/test/out/simple_out_2.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(2, new FileWriter(file), TREE1, TREE2);
        assertEquals(Arrays.asList(TREE1, TREE2), Parser.parseFile(file));
    }

    private static final String TREE3STR
        = "<null>\n"
        + "  <\"null\">";
    private static final GenericNode TREE3 = Parser.makeTree(TREE3STR, 2);

    private static final String TREE4STR
        = "<#, 2>\n"
        + "  <\\\"Hi\\\", \"Hello, world!\", How are you?>\n"
        + "    <>";
    private static final GenericNode TREE4 = Parser.makeTree(TREE4STR, 2);

//    @Test(timeout = TIMEOUT)
    public void testForestOut3() throws IOException {
        File file = new File("./src/test/out/simple_out_3.tml");
        if (!file.exists()) {
            file.createNewFile();
        }
        TMLOutput.writeMarkUpForest(2, new FileWriter(file), TREE1, TREE2,
                TREE3, TREE4);
        assertEquals(Arrays.asList(TREE1, TREE2, TREE3, TREE4),
                Parser.parseFile(file));
    }

}
