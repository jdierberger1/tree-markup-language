package parsefile;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import tml.parser.Parser;
import tml.tree.GenericNode;

public class ParseFileTests {

    private static final int TIMEOUT = 200;

    private static final String TREE1STR
        = "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "    <\"<>\">\n"
        + "    <12>\n"
        + "      <\"null\">\n"
        + "  <10.0>\n"
        + "    <10000000000L>";

    private static final GenericNode TREE1 = Parser.makeTree(TREE1STR, 2);

    @Test(timeout = TIMEOUT)
    public void testParseFile1() throws FileNotFoundException {
        List<GenericNode> list = Parser.parseFile(
                Paths.get("src", "test", "resources", "simple_tree_1.tml")
                .toFile());
        assertEquals(Arrays.asList(TREE1), list);
    }

    private static final String FOREST1STR
        = "#Comment\n"
        + "\n"
        + "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "      <4>\n"
        + "  <2>\n"
        + "\n"
        + "<2>\n"
        + "#Comment in the middle!\n"
        + "  <2>\n"
        + "\n"
        + "\n"
        + "#Comment\n"
        + "<1>\n"
        + "  <2>\n"
        + "    <3>\n"
        + "  <2>";

    private static final List<GenericNode> FOREST1
        = Parser.makeForest(FOREST1STR, 2);

    @Test(timeout = TIMEOUT)
    public void testParseFile2() throws FileNotFoundException {
        List<GenericNode> list = Parser.parseFile(
                Paths.get("src", "test", "resources", "multi_tree_1.tml")
                .toFile());
        assertEquals(FOREST1, list);
    }

    private static final String SPECNODE1STR
        = "# <data, height, balance factor>\n"
        + "\n"
        + "# data, then attributes\n"
        + "<B, 1, 0>\n"
        + "  <A, 0, 0>\n"
        + "  <C, 0, 0>\n";

    private static final List<GenericNode> FOREST2
        = Parser.makeForest(SPECNODE1STR, 2);

    @Test(timeout = TIMEOUT)
    public void testParseFile3() throws FileNotFoundException {
        List<GenericNode> list = Parser.parseFile(
                Paths.get("src", "test", "resources", "spec_node.tml")
                .toFile());
        assertEquals(FOREST2, list);
    }

    private static final String IPL1STR
        = "<1> \n"
        + "    <2>\n"
        + "        <>\n"
        + "        <3>\n"
        + "        <\"<>\">\n"
        + "        <null>\n"
        + "        <12>\n"
        + "            <\"null\">\n"
        + "    <10.0>\n"
        + "        <10000000000L>";

    private static final List<GenericNode> FOREST3
        = Parser.makeForest(IPL1STR, 4);

    @Test(timeout = TIMEOUT)
    public void testParseFile4() throws FileNotFoundException {
        List<GenericNode> list = Parser.parseFile(
                Paths.get("src", "test", "resources", "ipl.tml")
                .toFile());
        assertEquals(FOREST3, list);
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile5() throws FileNotFoundException {
        List<GenericNode> list = Parser.parseFile(
                Paths.get("src", "test", "resources", "empty1.tml")
                .toFile());
        assertEquals(Arrays.asList(), list);
    }

    @Test(timeout = TIMEOUT)
    public void testParseFile6() throws FileNotFoundException {
        List<GenericNode> list = Parser.parseFile(
                Paths.get("src", "test", "resources", "empty2.tml")
                .toFile());
        assertEquals(Arrays.asList(), list);
    }

}
